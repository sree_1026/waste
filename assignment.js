var data = "";
var num_arr = [];
var point_arr = [];
class Lines
{
    constructor(array_of_points){
        this.points = array_of_points;
    }
    find_distance(a,b){
        return Math.sqrt(Math.pow((b[0] - a[0]), 2) + Math.pow((b[1] - a[1]), 2));
    }
}

class Triangle extends Lines{
    constructor(array_of_points){
        super();
        this.a = array_of_points[0];
        this.b = array_of_points[1];
        this.c = array_of_points[2];
    }
    get_length(){
        var ab = Lines.prototype.find_distance(this.a, this.b);
        var bc = Lines.prototype.find_distance(this.b,this.c);
        var ca = Lines.prototype.find_distance(this.c,this.a);
        return Array.of(ab, bc, ca);
    }
    shape(sides){
        if(this.area() !== 0){
            if(sides[0] === sides[1] && sides[1] == sides[2]){
                return "Equilateral Triangle";
            }            
            else if(sides[0] === sides[1] || sides[1] == sides[2] || sides[2] == sides[0])
            {
                return "Isoceles Triangle";
            }
            else
            {
                return "Scalene Triangle"
            }
        }
        else{
            return "points are co-linear";
        }
    }
    area(){
        return Math.abs((this.a[0]*(this.b[1]-this.c[1])+this.b[0]*(this.c[1]-this.a[1])+this.c[0]*(this.a[1]-this.b[1]))/2);  
    }
}

class Qaudrilateral extends Lines{
    constructor(array_of_points){
        super();
        this.a = array_of_points[0];
        this.b = array_of_points[1];
        this.c = array_of_points[2];
        this.d = array_of_points[3];
    }
    shape(){
        var ab = Array.of((this.b[0] - this.a[0]), (this.b[1] - this.a[1]));
        var bc = Array.of((this.c[0] - this.b[0]), (this.c[1] - this.b[1]));
        var dc = Array.of((this.c[0] - this.d[0]), (this.c[1] - this.d[1]));
        var ad = Array.of((this.d[0] - this.a[0]), (this.d[1] - this.a[1]));
        var sc_prod_ab_ad = ab[0]*ad[0] + ab[1]*ad[1]; 
        var len_ab = Lines.prototype.find_distance(this.a, this.b);
        var len_bc = Lines.prototype.find_distance(this.b, this.c);
        if(ab.join() == dc.join() && ad.join()==bc.join())
        {
            //rhombus
            if(len_ab == len_bc && sc_prod_ab_ad == 0)
            {
                return "Square";
            }
            else if(len_ab == len_bc)
            {
                return "rhombus";
            }
            else if(sc_prod_ab_ad == 0)
            {
                return "rectangle";
            }
            else
            {
                return "parallelogram";
            }
        }
        else{
                return "trapezoid";
        }
    }
    area(){
        var area_abc = Math.abs((this.a[0]*(this.b[1]-this.c[1])+this.b[0]*(this.c[1]-this.a[1])+this.c[0]*(this.a[1]-this.b[1]))/2);
        var area_adc = Math.abs((this.a[0]*(this.d[1]-this.c[1])+this.d[0]*(this.c[1]-this.a[1])+this.c[0]*(this.a[1]-this.d[1]))/2);
        return area_abc+area_adc;
    }
    // shape(){
    //     var AB = Lines.prototype.find_distance(this.a, this.b);
    //     var BC = Lines.prototype.find_distance(this.b, this.c);
    //     var CD = Lines.prototype.find_distance(this.c, this.d);
    //     var DA = Lines.prototype.find_distance(this.d, this.a);
    //     var AC = Lines.prototype.find_distance(this.a, this.b);
    //     var BD = Lines.prototype.find_distance(this.b, this.d);
    //     if(AB === BC && AC === BD){
    //         return "Sqaure";
    //     }
    //     else if(AB === CD && BC === DA && AC === BD){
    //         return "Rectangle";
    //     }
    //     else if(AB === CD && BC === DA && AC !== BD){
    //         return "Parallelogram";
    //     }
    //     else if(AB === BC && AC === BD && AC !== BD){
    //         return "Rhombus";
    //     }
    //     else{
    //         return "Trapezium";
    //     }
    // }
}
function myfunction() {
    const x = document.getElementsByTagName("input");
    data = x[0].value;
    num_arr = data.split(" ");
    for (i = 0; i < num_arr.length; i++) {
        if (num_arr[i] != '[' && num_arr[i] != ']') {
            // console.log(JSON.parse(num_arr[i]));
            point_arr.push(JSON.parse(num_arr[i]));
        }
    }
    for (index=0; index<point_arr.length;index++){
        if(point_arr[index].length == 2){
            two_points(point_arr[index]);
        }
        else if(point_arr[index].length == 3){
            three_points(point_arr[index]);
        }
        else if(point_arr[index].length == 4){
            four_points(point_arr[index]);
        }
    }
    // distance_calculation(point_arr);
    // find_shape(point_arr);
}
function two_points(array_of_points){
    var line = new Lines(array_of_points);
    var distance = line.find_distance(array_of_points[0], array_of_points[1]);
    console.log(distance);
}
function three_points(array_of_points){
    var triangle = new Triangle(array_of_points);
    var sides = triangle.get_length();
    console.log("Shape of triangle: "+triangle.shape(sides));
    if(triangle.area()!==0){
        console.log("Aread of Triangle is: "+triangle.area());
    }
}
function four_points(arr_of_points){
    var quadrilateral = new Qaudrilateral(arr_of_points);
    console.log("Shape of quadrilateral: "+quadrilateral.shape());
    console.log("Area of Quadrilateral: "+quadrilateral.area());
}



// function distance_calculation(point_arr)
// {
//     var arr_of_points = point_arr[0].concat(point_arr[1]);
//     console.log("Distance Between Points: \n");
//     arr_of_points = arr_of_points.filter(function (element) {
//         return element !== undefined;
//     });
//     for (i = 0; i < arr_of_points.length - 1; i++) {
//         var sq = Math.sqrt(Math.pow((arr_of_points[i][0] - arr_of_points[i + 1][0]), 2) + Math.pow((arr_of_points[i][1] - arr_of_points[i + 1][1]), 2));
//         console.log(sq);
//     }
    
// }
// function find_shape(point_arr)
// {
//     console.log("in function find_shape")
//     var area=0;
//     for (var point_arr_index =0; point_arr_index<point_arr.length; point_arr_index++)
//     {
//         if(point_arr[point_arr_index].length === 2)
//         {
//             console.log("two points: \n");
//             var a = point_arr[point_arr_index][0];
//             var b = point_arr[point_arr_index][1] ;
//             var mid_point = [];
//             mid_point = Array.of(
//                 ((a[0]+b[0])/2) , ((a[1]+b[1])/2) 
//             );
//             if(mid_point[0]<0)
//             {
//                 if(mid_point[1]<0)
//                 {
//                     console.log("Mid point lies in III Quadrant");
//                 }
//                 else
//                 {
//                     console.log("Mid point lies in II Quadrant");
//                 }
//             }
//             else if(mid_point[1]<0)
//             {
//                 console.log("Mid point lies in IV Quadrant");
//             }
//             else
//             {
//                 console.log("Mid point lies in I Quadrant");
//             }
//         }
//         else if(point_arr[point_arr_index].length == 3){
//             console.log("Three points: ")
//             var a = point_arr[point_arr_index][0];
//             var b = point_arr[point_arr_index][1]
//             var c = point_arr[point_arr_index][2];
//             area = area_of_triangle(a, b, c);
//             console.log("area of triangle is: " + area);
//             if(area === 0){
//                 console.log("points are collinear");
//             }
//             else{
//                 var side_1 = Math.sqrt(Math.pow((b[0] - a[0]), 2) + Math.pow((b[1] - a[1]), 2)).toFixed(4);
//                 var side_2 = Math.sqrt(Math.pow((c[0] - a[0]), 2) + Math.pow((c[1] - a[1]), 2)).toFixed(4);
//                 var side_3 = Math.sqrt(Math.pow((c[0] - b[0]), 2) + Math.pow((c[1] - b[1]), 2)).toFixed(4);
//                 // console.log(side_1+ " "+ side_2 + " "+ side_3);
//                 if(side_1 === side_2 && side_1 === side_3)
//                 {
//                     console.log("Equilateral Triangle");
//                 }
//                 else if(side_1 === side_2 || side_2 == side_3 || side_1 == side_3)
//                 {
//                     console.log("Isoceles Triangle");
//                 }
//                 else
//                 {
//                     console.log("Scalene Triangle")
//                 }
//             }
//         }
//         else{
//             var a = point_arr[point_arr_index][0];
//             var b = point_arr[point_arr_index][1]
//             var c = point_arr[point_arr_index][2];
//             var d = point_arr[point_arr_index][3];
//             var ab = Array.of((b[0] - a[0]), (b[1] - a[1]));
//             var bc = Array.of((c[0] - b[0]), (c[1] - b[1]));
//             var dc = Array.of((c[0] - d[0]), (c[1] - d[1]));
//             var ad = Array.of((d[0] - a[0]), (d[1] - a[1]));
//             var sc_prod_ab_ad = ab[0]*ad[0] + ab[1]*ad[1]; 
//             var len_ab = Math.sqrt(Math.pow((b[0] - a[0]), 2) + Math.pow((b[1] - a[1]), 2));
//             var len_bc = Math.sqrt(Math.pow((b[0] - c[0]), 2) + Math.pow((b[1] - c[1]), 2));
//             if(ab.join() == dc.join() && ad.join()==bc.join())
//             {
//                 //rhombus
//                 if(len_ab == len_bc && sc_prod_ab_ad == 0)
//                 {
//                     console.log("Square");
//                 }
//                 else if(len_ab == len_bc)
//                 {
//                     console.log("rhombus");
//                 }
//                 else if(sc_prod_ab_ad == 0)
//                 {
//                     console.log("rectangle");
//                 }
//                 else
//                 {
//                     console.log("parallelogram");
//                 }
//             }
//             else{
//                 console.log("trapezoid");
//             }
//             area = area_of_triangle(a, b, c) + area_of_triangle(a, d, c)
//             console.log("area of quadrilateral is: "+area);
//         }
//     }
// }
// function area_of_triangle(x, y, z){
//     return Math.abs((x[0]*(y[1]-z[1])+y[0]*(z[1]-x[1])+z[0]*(x[1]-y[1]))/2);
// }

